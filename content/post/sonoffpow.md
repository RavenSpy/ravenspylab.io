---
title: Hacking Sonoff POW R1
date: 2018-07-27 10:59:23 +0000
tags:
- app
- inventory control
- school
- data base
categories:
- programming
description: Unlocking Sonoff POW R1 freatures and connecting with IoT dashboard.
cover: "/img/sonoffpow/pow.jpg"
draft: true

---
![IoT](/img/greenschool/iot.png)Currently, with the arrival of [IoT](https://en.wikipedia.org/wiki/Internet_of_things), many applications are changing the way they communicate with the world and with people. Technologies are already present in the most quotidian, and that includes schools.

---

# 1. Considerations:
Being a participant of GCI, I have learned a lot of things.Here are the few of the many things which I've learn't till date, thanks to **OpenWISP**(I'm still learning more :wink:):
## 1.1 Linux Command Line 
Ever since the beginning of GCI, I've been using **Lubuntu** for doing almost everything. Be it **installing packages**, **setting up a virtual machine**, **creating a webpage**, **using GitHub** or **hosting a local server**, I've got so used to the quick and efficient CLI of Linux now that I've developed a sort of disgust for using the mouse. I've also acquired great details about popularly used **shell commands**.


---

# Conclusion
Phew, I didn't think that my write-up would be this huge, looks like I still have a long way to go as a writer. 
Anyways, I just wanted to say that I'm really, **whole-heartedly** thankful to **Google Code-in** as well as **OpenWISP** for increasing my skill-set significantly. I never would've thought that I'd be able to learn python, host **my own** website and all the stuff which I've learnt in these **challenging yet fun**  forty days.
I really appreciate the guidance **OpenWISP** provided since I was a mere beginner at everything when I had planned to enter **Google Code-in**, but now I've leveled up :smile:. I've also made some great friends along the way with whom I love discussing programming-related stuff.
I really hope that one day I'll be able to improve my skills to that extent at which I'll be able to contribute to OpenWISP **as a mentor**. 

That's all that I have to to say. Actually, no :wink: ! I'd like to share a quote before that:

>  Everything seems difficult until and unless you give it a try.